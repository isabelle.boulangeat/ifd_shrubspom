require(ggplot2)
require(reshape2)
require(sjPlot)
plot.effects.ld <- function(Fit.mcmc, vars, nmetapars=1, colos = c("lightblue", "steelblue", "grey50", "tomato", "orange")){
  all.vars = rep(vars,nmetapars)
  df = as.data.frame(Fit.mcmc$Posterior1)
  df.long= melt(df)
  df.long$par = unlist(lapply(df.long$variable, substr, start=1, stop=2))
  df.long$index = unlist(lapply(df.long$variable, gsub, pattern="[^(0-9)]", replacement=""))
  df.long2 = df.long[df.long$index>1,]
  df.long2$var = factor(df.long2$index)
  levels(df.long2$var) = all.vars
  levels(df.long2$var) = vars
  df.long2$par = factor(df.long2$par)
  levels(df.long2$par)
 # = c("aC", "aD", "bC", "bD", "d")
  p1 = ggplot(df.long2, aes(x = var, y = value, fill = var)) +
  geom_violin(trim= FALSE) +
  geom_boxplot(alpha=0.3, width=0.2, outlier.size = 0) +
  facet_wrap(~ par, ncol=3, dir="v", scales="free") +
  theme(axis.title.x=element_blank(),
       axis.text.x=element_blank(),
       axis.ticks.x=element_blank())
  p2 = p1 + geom_hline(yintercept=0, linetype="dashed", color = "black") +
       labs(title="Posterior distributions", x="variable", y="effect") +
         scale_fill_manual(name="variables",
                            labels=levels(df.long2$var) ,
                            values = colos)

  return(p2)
}

#-------------------------
plot.effects.glm <- function(SPECIES, glm.fit, colos = c("lightblue", "steelblue", "grey50", "tomato", "orange")){
  coeff.colo = summary(glm.fit$colo)$coefficients[-1,1]
  vars.colo = rownames(glm.fit$colo$coeff)[-1]
  plot_model(glm.fit$colo, transform = NULL, show.values=TRUE, title = SPECIES)
  plot_model(glm.fit$colo, transform = NULL, show.values=TRUE, title = SPECIES)

  return(barplot(coeff.colo, col = colos))
}
