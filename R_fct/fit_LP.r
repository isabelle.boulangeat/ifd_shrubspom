
test_model<- function(MyData, loglik_fct){
  require(LaplacesDemon)
  set.seed(129837)
  Initial.Values <- rnorm(length(MyData$parm.names), 0, 1)

  res = loglik_fct(Initial.Values, MyData)$LP
  return(res)
}

first.fit <- function(MyData, loglik_fct){

  set.seed(666)
  Initial.Values <- rnorm(length(MyData$parm.names), 0, 1)
  Fit = LaplaceApproximation(Model=loglik_fct, Initial.Values, Data=MyData, Method="HAR", Iterations = 50000)

  Initial.Values <- as.initial.values(Fit)
  set.seed(666)
  Fit.RDMH1 <- LaplacesDemon(Model=loglik_fct, Data=MyData, Initial.Values,
      Covar=NULL, Iterations=1000, Status=100, Thinning=1,
      Algorithm="RDMH", Specs=NULL)
  return(Fit.RDMH1)

}

second.fit <- function(MyData, ffit, loglik_fct){
  Initial.Values <- as.initial.values(ffit)
  set.seed(666)
  sfit <- LaplacesDemon(loglik_fct, Data=MyData, Initial.Values,
       Covar=ffit$Covar, Iterations=30000, Status=8333, Thinning=30,
       Algorithm="RDMH", Specs=NULL)
  return(sfit)
}

third.fit <- function(MyData, sfit, loglik_fct){
  Initial.Values <- as.initial.values(sfit)
  set.seed(666)
  tfit <- LaplacesDemon(loglik_fct, Data=MyData, Initial.Values,
       Covar=sfit$Covar, Iterations=830000, Status=11333, Thinning=830,
       Algorithm="RDMH", Specs=NULL)
  return(tfit)
}
