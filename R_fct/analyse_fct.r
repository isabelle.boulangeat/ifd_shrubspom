require(dplyr)
require(MASS)
require(fmsb)
require(ROCR)
require(LaplacesDemon)
#-----------------------------------

load.datasets <- function(){
  temp = read.table("shrubhub/ARCfunc_timeseries_temp.txt", h=T, sep="\t", encoding = "UTF-8", comment.char="", quote = "")
  precip = read.table("shrubhub/ARCfunc_timeseries_prec.txt", h=T, sep="\t", encoding = "UTF-8", comment.char="", quote = "")

  climato = read.table("shrubhub/TVC_SITE_SUBSITE_UPDATED2016_snow_GSL_GST_CHELSAclimatologies.txt", h=T, sep="\t", encoding = "UTF-8", comment.char="", quote = "")
  climato$SiteSubsite = paste(climato$SITE, climato$SUBSITE, sep=":")

  load("coverc_sub.rdata")

  cover.sub = coverc.sub[which(coverc.sub$TRTMT =="CTL"),]
  cover.sub = cover.sub[which(cover.sub$RepeatedPlots ==1),]

  cover.dom = cover.sub[which(cover.sub$YEAR<2014),]

  return(list(cover=cover.dom, climato=climato))
}

#-----------------------------------

dat.species <- function(SPECIES, DATA, Bayesian=TRUE){
  spdat = create_dataset(SPECIES, DATA$cover, DATA$climato)
  spdat$GSL_anom = spdat$GSL-spdat$GSL_av
  spdat$GST_anom = spdat$GST-spdat$GST_av
  spdat$snowDays_anom = spdat$snowDays-spdat$snow_days_av
  spdat$trType = factor(spdat$trType)
  levels(spdat$trType) <- c("AA", "COLO", "EXT", "PP")
  spdat = spdat[spdat$interval<10,]

  selectVars = c("snowDays", "GSL", "GST", "SoilMoist", "TotalCover")
  spdat$SoilMoist = factor(spdat$SoilMoist)
  levels(spdat$SoilMoist) = c(-1,0,1)
  spdat$SoilMoist = as.numeric(as.character(spdat$SoilMoist))

  spdat[which(spdat$trType%in%c("PP", "EXT")),"subset"] = "colo"
  dat.ext = scale(spdat[which(spdat$trType%in%c("PP", "EXT")),selectVars])
  dat.ext[,"SoilMoist"] = spdat[which(spdat$trType%in%c("PP", "EXT")),"SoilMoist"]

  spdat[which(spdat$trType%in%c("AA", "COLO")),"subset"] = "ext"
  dat.colo = scale(spdat[which(spdat$trType%in%c("AA", "COLO")),selectVars])
  dat.colo[,"SoilMoist"] = spdat[which(spdat$trType%in%c("AA", "COLO")),"SoilMoist"]

  if(Bayesian){
    nvars = length(selectVars)
    parm.names.ext = as.parm.names(list(aE=rep(0, 1+nvars)))
    nbetas.ext = length(parm.names.ext)
    MyData.ext <- list(N = nrow(dat.ext), dat=dat.ext, mon.names = c("logLik") , parm.names= parm.names.ext,
    var.aE = 1:(nvars),
    aE.pos = grep("aE", parm.names.ext),
    PA.pos = spdat$trType[which(spdat$trType%in%c("PP", "EXT"))]=="EXT",
    PP.pos = spdat$trType[which(spdat$trType%in%c("PP", "EXT"))]=="PP",
    invlogit_ = invlogit_,
    itime = spdat$interval[which(spdat$trType%in%c("PP", "EXT"))],
    nbetas = nbetas.ext)

    parm.names.colo = as.parm.names(list(aC=rep(0, 1+nvars)))
    nbetas.colo = length(parm.names.colo)
    MyData.colo<- list(N = nrow(dat.colo), dat=dat.colo, mon.names = c("logLik") , parm.names= parm.names.colo,
    var.aC = 1:(nvars),
    aC.pos = grep("aC", parm.names.colo),
    AP.pos = spdat$trType[which(spdat$trType%in%c("AA", "COLO"))]=="COLO",
    AA.pos = spdat$trType[which(spdat$trType%in%c("AA", "COLO"))]=="AA",
    invlogit_ = invlogit_,
    density = rep(1, nrow(dat.colo)),
    itime = spdat$interval[which(spdat$trType%in%c("AA", "COLO"))],
    nbetas = nbetas.colo)

    LD = list(colo = MyData.colo, ext = MyData.ext)
  } else (LD = NULL)

    dat.colo = data.frame(dat.colo)
    dat.colo$colo.event = as.numeric(MyData.colo$AP.pos)

    dat.ext = data.frame(dat.ext)
    dat.ext$ext.event = as.numeric(MyData.ext$PA.pos)

  return(list(spdat = spdat,LD = LD, colo = dat.colo, ext = dat.ext, sp = SPECIES))
}

#-----------------------------------
check.nas <- function(spDATA){
  print(spDATA$sp)
  return(sum(is.na(spDATA$spdat$GST)))
}
#-----------------------------------

fit.species <- function(spDATA){
    print(spDATA$sp)

    colo.mod = glm(colo.event~ (snowDays + GSL + GST + TotalCover) * SoilMoist, family = "binomial", data = spDATA$colo)
    stepMod.colo  = stepAIC(colo.mod, direction = "both", trace=0)

    ext.mod = glm(ext.event~  (snowDays + GSL + GST + TotalCover) * SoilMoist, family = "binomial", data = spDATA$ext)
    stepMod.ext  = stepAIC(ext.mod, direction = "both", trace=0)

    return(list(colo = stepMod.colo, ext = stepMod.ext, dat = spDATA))
}

#-----------------------------------

eval.fit <- function(stepMod, data, response.var){
  pred = predict(stepMod,new=data,"response")
  R2 = NagelkerkeR2(stepMod)$R2
  perf = performance(prediction(pred, data[,response.var]), "auc")
  AUC = perf@y.values[[1]]
  return(list(pred = pred, R2=R2, AUC=AUC))
}
#-----------------------------------
predictions <- function(glm.fit, pred.data, selectVars){
  #pred.data$SoilMoist = factor(pred.data$SoilMoist)
  #levels(pred.data$SoilMoist) = c(-1, 0 , 1)
  #pred.data$SoilMoist = as.numeric(as.character(pred.data$SoilMoist))
  pred.data= glm.fit$dat$spdat
  sc.colo = scale(glm.fit$dat$spdat[which(glm.fit$dat$spdat$trType%in%c("AA", "COLO")),selectVars])
  dat.pred.colo = scale(pred.data[,selectVars], center = attr(sc.colo,"scaled:center"), scale = attr(sc.colo,"scaled:scale"))

  sc.ext = scale(glm.fit$dat$spdat[which(glm.fit$dat$spdat$trType%in%c("PP", "EXT")),selectVars])
  dat.pred.ext = scale(pred.data[,selectVars], center = attr(sc.ext,"scaled:center"), scale = attr(sc.ext,"scaled:scale"))

  pred.colo = predict(glm.fit$colo,new=data.frame(dat.pred.colo),"response")
  pred.ext = predict(glm.fit$ext,new=data.frame(dat.pred.ext),"response")
  pred.lambda = 1- (pred.ext/pred.colo)

  return(list(lambda = pred.lambda, sp = glm.fit$dat$sp, YEAR = pred.data$YEAR, SUBSITE = pred.data$SUBSITE))
}


#-----------------------------------

fit.species.LD <- function(spDATA){

}
