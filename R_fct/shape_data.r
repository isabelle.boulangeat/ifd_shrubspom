#
# merge_distribution <- function(sp.distri.path, ncell.side=3, plots, plot.diag = FALSE){
# 	require(raster)
# 	sp.distri = raster(sp.distri.path)
# 	# 3x3 mean filter
# 	sp.distri2 <- focal(sp.distri, w=matrix(1/(ncell.side*ncell.side),nrow=ncell.side,ncol=ncell.side), na.rm = TRUE)
# 	require(sp)
# 	plots.sp = unique(plots[,c("SUBSITE", "Latitude", "Longitude")])
# 	coordinates(plots.sp) = ~Longitude+Latitude
# 	projection(plots.sp) = CRS("+proj=longlat +datum=WGS84")
# 	plots.proj = spTransform(plots.sp, projection(sp.distri2))
# 	distri = data.frame(SUBSITE = plots.proj$SUBSITE, distriArea = extract(sp.distri2, plots.proj, method="simple"), Longitude = plots.sp$Longitude, Latitude = plots.sp$Latitude)
# 	# nb : some plots are not in the arctic and generate a NaN value
# 	if(plot.diag ==TRUE){
# 		pdf("diag_distri.pdf")
# 		plot(sp.distri2)
# 		points(plots.proj)
# 		dev.off()
# 	}
#
# 	return(distri)
# }


#-----------

create_dataset <- function(SPECIES, cover.dom, climato){

# Plots
# only subsites where the species have been seen
subsites = unique(cover.dom$SUBSITE[which(cover.dom$Name==SPECIES)])
cover.sp = cover.dom[which(cover.dom$SUBSITE %in% subsites),]
sites = unique(cover.sp[,c("SemiUniquePLOT", "YEAR", "SUBSITE")])

# table where sp considered as present (abundance filter)
sp.table = cover.sp[which(cover.sp$Name ==SPECIES),c("SemiUniquePLOT", "YEAR", "COVER")]
sp.table$PA = ifelse(sp.table$COVER>0, 1, 0)

# long table with presences and absences
long.tab = merge(sites, sp.table, by = c("SemiUniquePLOT", "YEAR"), all.x=TRUE)
long.tab$PA[is.na(long.tab$PA)] = 0

#sp.table.by.site = split(cover.sp, cover.sp$SemiUniquePLOT)

# build transition table # need cover.sp
sp.table.tr.list = lapply(unique(long.tab$SemiUniquePLOT), function(x){
# x = unique(long.tab$SemiUniquePLOT)[10]
	p = long.tab[which(long.tab$SemiUniquePLOT==x),]
	# p = cover.sp[which(cover.sp$SemiUniquePLOT%in%x$SemiUniquePLOT),] # all years
	# cover = merge(p[which(p$Name==SPECIES),c("SemiUniquePLOT", "YEAR","COVER")],
	# 	unique(p[,c("SemiUniquePLOT", "YEAR")]), all=TRUE)
	# #cover[which(is.na(cover$COVER)), "COVER"]=0

	ntransitions = length(unique(p$YEAR))-1
	df = data.frame(SemiUniquePLOT = rep(unique(p$SemiUniquePLOT), each =ntransitions),
	y0 =sort(unique(p$YEAR))[-length(unique(p$YEAR))],
	y1 =sort(unique(p$YEAR))[-1]
	)
	df$st0 =merge(df, p[, c("YEAR", "PA")],  by.x = "y0", by.y = "YEAR")$PA
	df$st1 =merge(df, p[, c("YEAR", "PA")],  by.x = "y1", by.y = "YEAR")$PA
	df
	return(df)
})
sp.table.tr= do.call(rbind, sp.table.tr.list)


# merge with plot info
sp.table.tr.all = unique(merge(sp.table.tr, cover.sp[, c("SemiUniquePLOT", "YEAR", "SoilMoist", "TotalCover")], by.x = c("SemiUniquePLOT", "y0"),by.y= c("SemiUniquePLOT", "YEAR")))

# intervals
sp.table.tr.all$interval = sp.table.tr.all$y1 - sp.table.tr.all$y0

# explo colo/ extinctions
sp.table.tr.all$trType = "present"
sp.table.tr.all$trType[which(sp.table.tr.all$st0==0 & sp.table.tr.all$st1==1)]="colonisation"
sp.table.tr.all$trType[which(sp.table.tr.all$st0==1 & sp.table.tr.all$st1==0)]="extinction"
sp.table.tr.all$trType[which(sp.table.tr.all$st0==0 & sp.table.tr.all$st1==0)]="absent"


# extract plot info

#--------------------------------------------------
# combine with climate (5 years earlier average)
#---------------------------------------------------
dataset = proj.dat.shape(sites, climato)
#--------------------------------------------------
# merge datasets
#---------------------------------------------------
#dataset = merge(climData, sites, by=c("SUBSITE", "YEAR"))
dataset2 = merge(dataset, sp.table.tr.all,by.x = c("SemiUniquePLOT", "YEAR"), by.y = c("SemiUniquePLOT", "y0"), all.y=TRUE)
#nrow(dataset2)
#nrow(dataset)

#dataset2 = merge(dataset, distri, by = "SUBSITE", all.x=TRUE, all.y=FALSE)
#if(!(nrow(dataset2) == n1)) stop("error merging datasets")

# dataset3 = merge(dataset2, all_means, by = "SUBSITE", all.x=TRUE, all.y=FALSE)
#if(!(nrow(dataset3) == n1)) stop("error merging datasets")

return(dataset2)
}
#--------------
# clim arrange data function
grepClim <- function(climData, climato, variable, nyears){
	clim = apply(climData, 1, function(x){
		subClim = climato[which(climato$SiteSubsite==x[[1]]),]
		index = as.numeric(sapply(1:nyears, function(k){
		  res = grep(paste(variable,as.numeric(x[[2]])-k+1,sep=""), names(subClim),  value=FALSE)
		  return(res)
		}))
		
		if(sum(is.na(index))>0) {
		  meanClim = NA
		} else meanClim = mean(as.numeric(subClim[index]))
		return(meanClim)
	})
	return(clim)
}
##-------------
proj.dat.shape <- function(sites, climato){
	climData = unique(sites[,c("SUBSITE","YEAR")])
	climData$snowDays = grepClim(climData, climato, "snow_days_", 5)
	climData$GSL = grepClim(climData, climato, "GSL_", 5)
	climData$GST = grepClim(climData, climato, "GST_", 5)

	# ------------------------------------------------
	# average
	#------------------------------------------------

	mean.snowDays = sapply(climato$SiteSubsite, function(x){
		subClim = climato[which(climato$SiteSubsite==x),]
		snowDays = mean(as.numeric(subClim[sapply(1979:2013, function(k){grep(paste("snow_days_",k,sep=""), names(subClim),  value=FALSE)})]))
		return(snowDays)
	})
	mean.GSL = sapply(climato$SiteSubsite, function(x){
		subClim = climato[which(climato$SiteSubsite==x),]
		GSL = mean(as.numeric(subClim[sapply(1979:2013, function(k){grep(paste("GSL_",k,sep=""), names(subClim),  value=FALSE)})]))
		return(GSL)
	})
	mean.GST = sapply(climato$SiteSubsite, function(x){
		subClim = climato[which(climato$SiteSubsite==x),]
		GST = mean(as.numeric(subClim[sapply(1979:2013, function(k){grep(paste("GST_",k,sep=""), names(subClim),  value=FALSE)})]))
		return(GST)
	})

	mean.snowDays = unique(data.frame(snow_days_av=mean.snowDays, SUBSITE = names(mean.snowDays)))
	mean.GSL = unique(data.frame(GSL_av=mean.GSL, SUBSITE = names(mean.GSL)))
	mean.GST = unique(data.frame(GST_av=mean.GST, SUBSITE = names(mean.GST)))
	require(dplyr)
	all_means <- mean.snowDays %>% inner_join(mean.GSL, by = "SUBSITE") %>% inner_join(mean.GST, by ="SUBSITE")

	#--------------------------------------------------
	# merge datasets
	#---------------------------------------------------
	dataset = merge(climData, sites, by=c("SUBSITE", "YEAR"))

	dataset2 = merge(dataset, all_means, by = "SUBSITE", all.x=TRUE, all.y=FALSE)
	#if(!(nrow(dataset3) == n1)) stop("error merging datasets")

  return(dataset2)
}
