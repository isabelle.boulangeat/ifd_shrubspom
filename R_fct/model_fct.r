# parm = Initial.Values
# Data = MyData
loglik_fct <- function(parm, Data)
{
  require(data.table)

  aC = parm[Data$aC.pos]
  aE = parm[Data$aE.pos]


  # fit transitions
  logit_alphaC = tcrossprod(aC, as.matrix(cbind(rep(1, Data$N), Data$dat[,Data$var.aC])))
  logit_alphaE = tcrossprod(aE, as.matrix(cbind(rep(1, Data$N), Data$dat[,Data$var.aE])))

  # compute transitions accounting for interval time and logit transformation
  # ! might be NaN because exp(bigNumber)
  aC.fct=as.vector(Data$invlogit_(logit_alphaC, Data$itime))
  aE.fct=as.vector(Data$invlogit_(logit_alphaE, Data$itime))

  N = Data$density

  # LIKELIHOOD
  lik = rep(NA, nrow(Data$dat))

  lik[which(Data$AA.pos)]= (1-(aC.fct*N)) [Data$AA.pos]
  lik[which(Data$AP.pos)]= (N*aC.fct) [Data$AP.pos]
  lik[which(Data$PA.pos)]= (aE.fct) [Data$PA.pos]
  lik[which(Data$PP.pos)]= (1-aE.fct) [Data$PP.pos]

  lik[lik==0] = .Machine$double.xmin
  loglik = sum(log(lik))


  # PRIOR
  # prior cauchy following Gelman 2008
  # intercepts different of slope
  beta.aC = dcauchy(aC[1], 1, 10, log = TRUE) + sum(dcauchy(aC[-1], 0, 2.5, log = TRUE))
  beta.aE = dcauchy(aE[1], 1, 10, log = TRUE) + sum(dcauchy(aE[-1], 0, 2.5, log = TRUE))

  # POSTERIOR
  LP = loglik + beta.aE + beta.aC

  return(list(LP = LP, Dev = -2*loglik, Monitor = LP, yhat = lik,parm = parm))
}


invlogit_ <- function(x, itime)
{
  expx = ifelse(exp(x)==Inf, .Machine$double.xmax, exp(x))
  proba = expx/(1+expx)
  proba_itime = 1 - (1 - proba)^(itime)
  return(proba_itime)
}


ext_model <- function(parm, Data)
{
  require(data.table)
  aE = parm[Data$aE.pos]
  # fit transitions
  logit_alphaE = tcrossprod(aE, as.matrix(cbind(rep(1, Data$N), Data$dat[,Data$var.aE])))

  # compute transitions accounting for interval time and logit transformation
  # ! might be NaN because exp(bigNumber)
  aE.fct=as.vector(Data$invlogit_(logit_alphaE, Data$itime))

  # LIKELIHOOD
  lik = rep(NA, nrow(Data$dat))
  lik[which(Data$PA.pos)]= (aE.fct) [Data$PA.pos]
  lik[which(Data$PP.pos)]= (1-aE.fct) [Data$PP.pos]

  lik[lik==0] = .Machine$double.xmin
  loglik = sum(log(lik))


  # PRIOR
  # prior cauchy following Gelman 2008
  # intercepts different of slope
  beta.aE = dcauchy(aE[1], 1, 10, log = TRUE) + sum(dcauchy(aE[-1], 0, 2.5, log = TRUE))

  # POSTERIOR
  LP = loglik + beta.aE

  return(list(LP = LP, Dev = -2*loglik, Monitor = LP, yhat = lik,parm = parm))
}

colo_model <- function(parm, Data)
{
  require(data.table)

  aC = parm[Data$aC.pos]

  # fit transitions
  logit_alphaC = tcrossprod(aC, as.matrix(cbind(rep(1, Data$N), Data$dat[,Data$var.aC])))

  # compute transitions accounting for interval time and logit transformation
  # ! might be NaN because exp(bigNumber)
  aC.fct=as.vector(Data$invlogit_(logit_alphaC, Data$itime))

  N = Data$density

  # LIKELIHOOD
  lik = rep(NA, nrow(Data$dat))

  lik[which(Data$AA.pos)]= (1-(aC.fct*N)) [Data$AA.pos]
  lik[which(Data$AP.pos)]= (N*aC.fct) [Data$AP.pos]

  lik[lik==0] = .Machine$double.xmin
  loglik = sum(log(lik))


  # PRIOR
  # prior cauchy following Gelman 2008
  # intercepts different of slope
  beta.aC = dcauchy(aC[1], 1, 10, log = TRUE) + sum(dcauchy(aC[-1], 0, 2.5, log = TRUE))

  # POSTERIOR
  LP = loglik +  beta.aC

  return(list(LP = LP, Dev = -2*loglik, Monitor = LP, yhat = lik,parm = parm))
}
