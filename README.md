---
title: "Main_analyse.Rmd"
author: "Isabelle Boulangeat"
date: "23/01/2020"
output: 
  html_document:
    keep_md: yes
    variant: markdown_github
editor_options: 
  chunk_output_type: console
always_allow_html: true
---



## Load data
- Load climate
- Load species cover data
- Filter out treatment plots
- filter out un-repeated plots
- filter out years > 2014 because of missing in climatologies


```r
DATA = load.datasets()
str(DATA$cover)
```

```
## 'data.frame':	32793 obs. of  43 variables:
##  $ SUBSITE             : chr  "ALEXFIORD:CASSIOPE" "ALEXFIORD:CASSIOPE" "ALEXFIORD:CASSIOPE" "ALEXFIORD:CASSIOPE" ...
##  $ SemiUniquePLOT      : chr  "ALEXFIORD:CASSIOPE_Cas.c.c.10new_CTL" "ALEXFIORD:CASSIOPE_Cas.c.c.10new_CTL" "ALEXFIORD:CASSIOPE_Cas.c.c.10new_CTL" "ALEXFIORD:CASSIOPE_Cas.c.c.10new_CTL" ...
##  $ YEAR                : num  1995 1995 1995 1995 1995 ...
##  $ Name                : chr  "Cassiope tetragona" "Papaver radicatum" "Oxyria digyna" "Luzula confusa" ...
##  $ SITE                : chr  "ALEXFIORD" "ALEXFIORD" "ALEXFIORD" "ALEXFIORD" ...
##  $ TRTMT               : chr  "CTL" "CTL" "CTL" "CTL" ...
##  $ PLOT                : chr  "Cas.c.c.10new" "Cas.c.c.10new" "Cas.c.c.10new" "Cas.c.c.10new" ...
##  $ GFNARROWwalker      : chr  "SEVER" "FORB" "FORB" "RUSH" ...
##  $ GFNARROWarft        : chr  "ESHRUB" "FORBSV" "FORBSV" "GRAMINOID" ...
##  $ COVER               : num  22 0 1 0 6 3 7 5 0 8 ...
##  $ UniquePLOT          : chr  "ALEXFIORD:CASSIOPE_Cas.c.c.10new_CTL_1995" "ALEXFIORD:CASSIOPE_Cas.c.c.10new_CTL_1995" "ALEXFIORD:CASSIOPE_Cas.c.c.10new_CTL_1995" "ALEXFIORD:CASSIOPE_Cas.c.c.10new_CTL_1995" ...
##  $ NumSurveysPerSubsite: int  3 3 3 3 3 3 3 3 3 3 ...
##  $ NumSurveysPerPlot   : int  3 3 3 3 3 3 3 3 3 3 ...
##  $ MinYearSubsite      : num  1995 1995 1995 1995 1995 ...
##  $ MaxYearSubsite      : num  2007 2007 2007 2007 2007 ...
##  $ DurationSubsite     : num  13 13 13 13 13 13 13 13 13 13 ...
##  $ MinYearPlot         : num  1995 1995 1995 1995 1995 ...
##  $ MaxYearPlot         : num  2007 2007 2007 2007 2007 ...
##  $ DurationPlot        : num  13 13 13 13 13 13 13 13 13 13 ...
##  $ SiteOnly            : chr  "ALEXFIORD" "ALEXFIORD" "ALEXFIORD" "ALEXFIORD" ...
##  $ SubsiteOnly         : chr  "CASSIOPE" "CASSIOPE" "CASSIOPE" "CASSIOPE" ...
##  $ MultipleSubsites    : num  1 1 1 1 1 1 1 1 1 1 ...
##  $ RepeatedSubsites    : num  1 1 1 1 1 1 1 1 1 1 ...
##  $ RepeatedPlots       : num  1 1 1 1 1 1 1 1 1 1 ...
##  $ MixedPlots          : num  0 0 0 0 0 0 0 0 0 0 ...
##  $ Morphosp            : num  0 0 0 0 0 0 0 0 1 0 ...
##  $ Genus               : chr  "Cassiope" "Papaver" "Oxyria" "Luzula" ...
##  $ Family              : chr  "Ericaceae" "Papaveraceae" "Polygonaceae" "Juncaceae" ...
##  $ SppInPlot           : num  1 1 1 1 1 1 1 1 1 1 ...
##  $ TotalCover          : num  44 44 44 44 44 44 44 44 44 46 ...
##  $ RelCover            : num  0.5 0 0.0227 0 0.1364 ...
##  $ Latitude            : num  78.9 78.9 78.9 78.9 78.9 ...
##  $ Longitude           : num  -75.8 -75.8 -75.8 -75.8 -75.8 ...
##  $ MaxTemp             : num  70.5 70.5 70.5 70.5 70.5 ...
##  $ MAT                 : num  -164 -164 -164 -164 -164 ...
##  $ MinTemp             : num  -367 -367 -367 -367 -367 ...
##  $ WarmQuarterTemp     : num  25.6 25.6 25.6 25.6 25.6 ...
##  $ ColdQuarterTemp     : num  -321 -321 -321 -321 -321 ...
##  $ WClimGrid           : num  57684506 57684506 57684506 57684506 57684506 ...
##  $ CRUGrid             : num  16049 16049 16049 16049 16049 ...
##  $ WClimGroup          : num  2 2 2 2 2 2 2 2 2 2 ...
##  $ SoilMoist           : chr  "MOIST" "MOIST" "MOIST" "MOIST" ...
##  $ WClimGroupSM        : num  3 3 3 3 3 3 3 3 3 3 ...
```

```r
str(DATA$climato)
```

```
## 'data.frame':	257 obs. of  172 variables:
##  $ SITE                                : Factor w/ 62 levels "ABISKO","AKUREYRI",..: 3 3 3 3 3 3 3 3 3 38 ...
##  $ SUBSITE                             : Factor w/ 254 levels "A","ABISKODRY",..: 1 37 46 64 67 68 82 122 123 215 ...
##  $ COMMTYPE                            : Factor w/ 5 levels "","DRY","MIXED",..: 5 5 5 5 2 2 5 2 2 4 ...
##  $ LAT                                 : num  78.9 78.9 78.9 78.9 78.9 ...
##  $ LONG                                : num  -75.7 -75.7 -75.7 -75.7 -75.9 ...
##  $ Checked_Coords                      : Factor w/ 2 levels "","x": 1 1 1 1 2 2 1 2 2 2 ...
##  $ HasCover                            : int  0 0 0 0 1 1 0 1 1 1 ...
##  $ ELEV                                : int  30 30 30 30 540 540 30 540 540 580 ...
##  $ AZONE                               : Factor w/ 4 levels "ALPINE","ANT",..: 3 3 3 3 3 3 3 3 3 4 ...
##  $ PI                                  : Factor w/ 42 levels "","Blok","COOPER",..: 12 12 12 12 12 12 12 22 22 24 ...
##  $ DomGrazer                           : Factor w/ 10 levels "","LARGE","Small",..: 8 8 8 8 8 8 8 5 5 7 ...
##  $ GrazerIntensity                     : Factor w/ 5 levels "","LOW","high",..: 4 4 4 4 4 4 4 4 4 5 ...
##  $ CAVM                                : Factor w/ 22 levels "","B1","B3","B4",..: 21 21 21 21 4 3 21 4 3 8 ...
##  $ CAVMBROAD                           : Factor w/ 7 levels "","B","G","P",..: 7 7 7 7 2 2 7 2 2 3 ...
##  $ PlotSize_m2                         : Factor w/ 16 levels "","0.04","0.0531",..: 1 1 1 1 12 12 1 16 16 6 ...
##  $ SurveyMethod                        : Factor w/ 13 levels "","BraunBlanquet",..: 4 4 4 4 8 8 4 3 3 2 ...
##  $ HitsPerPlot                         : Factor w/ 10 levels "","100","137",..: 1 1 1 1 2 2 1 NA NA NA ...
##  $ Comments                            : Factor w/ 20 levels "","\"0.25 m2 plots in 1992; 0.49 m2 in 2009\"",..: 1 1 1 1 1 1 1 2 2 1 ...
##  $ CHELSA_snow_days_1979               : int  365 365 365 365 365 365 365 365 365 365 ...
##  $ CHELSA_snow_days_1980               : int  365 365 365 365 365 365 365 365 365 298 ...
##  $ CHELSA_snow_days_1981               : int  365 365 365 365 365 365 365 365 365 365 ...
##  $ CHELSA_snow_days_1982               : int  365 365 365 365 365 365 365 365 365 365 ...
##  $ CHELSA_snow_days_1983               : int  365 365 365 365 365 365 365 365 365 365 ...
##  $ CHELSA_snow_days_1984               : int  365 365 365 365 365 365 365 365 365 321 ...
##  $ CHELSA_snow_days_1985               : int  365 365 365 365 365 365 365 365 365 271 ...
##  $ CHELSA_snow_days_1986               : int  365 365 365 365 365 365 365 365 365 365 ...
##  $ CHELSA_snow_days_1987               : int  365 365 365 365 365 365 365 365 365 311 ...
##  $ CHELSA_snow_days_1988               : int  365 365 365 365 365 365 365 365 365 312 ...
##  $ CHELSA_snow_days_1989               : int  365 365 365 365 365 365 365 365 365 365 ...
##  $ CHELSA_snow_days_1990               : int  365 365 365 365 365 365 365 365 365 340 ...
##  $ CHELSA_snow_days_1991               : int  365 365 365 365 365 365 365 365 365 334 ...
##  $ CHELSA_snow_days_1992               : int  365 365 365 365 365 365 365 365 365 365 ...
##  $ CHELSA_snow_days_1993               : int  365 365 365 365 355 355 365 355 355 355 ...
##  $ CHELSA_snow_days_1994               : int  365 365 365 365 365 365 365 365 365 351 ...
##  $ CHELSA_snow_days_1995               : int  365 365 365 365 365 365 365 365 365 365 ...
##  $ CHELSA_snow_days_1996               : int  365 365 365 365 365 365 365 365 365 292 ...
##  $ CHELSA_snow_days_1997               : int  365 365 365 365 365 365 365 365 365 317 ...
##  $ CHELSA_snow_days_1998               : int  365 365 365 365 346 346 365 346 346 365 ...
##  $ CHELSA_snow_days_1999               : int  365 365 365 365 354 354 365 354 354 308 ...
##  $ CHELSA_snow_days_2000               : int  365 365 365 365 365 365 365 365 365 311 ...
##  $ CHELSA_snow_days_2001               : int  365 365 365 365 365 365 365 365 365 307 ...
##  $ CHELSA_snow_days_2002               : int  365 365 365 365 365 365 365 365 365 306 ...
##  $ CHELSA_snow_days_2003               : int  365 365 365 365 365 365 365 365 365 303 ...
##  $ CHELSA_snow_days_2004               : int  365 365 365 365 365 365 365 365 365 299 ...
##  $ CHELSA_snow_days_2005               : int  365 365 365 365 316 316 365 316 316 328 ...
##  $ CHELSA_snow_days_2006               : int  365 365 365 365 340 340 365 340 340 318 ...
##  $ CHELSA_snow_days_2007               : int  338 338 338 338 324 324 338 324 324 354 ...
##  $ CHELSA_snow_days_2008               : int  334 334 334 334 297 297 334 297 297 302 ...
##  $ CHELSA_snow_days_2009               : int  303 303 303 303 298 298 303 298 298 342 ...
##  $ CHELSA_snow_days_2010               : int  365 365 365 365 344 344 365 344 344 283 ...
##  $ CHELSA_snow_days_2011               : int  313 313 313 313 306 306 313 306 306 365 ...
##  $ CHELSA_snow_days_2012               : int  312 312 312 312 295 295 312 295 295 337 ...
##  $ CHELSA_snow_days_2013               : int  365 365 365 365 365 365 365 365 365 365 ...
##  $ GSL_1979                            : int  0 0 0 0 0 0 0 0 0 0 ...
##  $ GSL_1980                            : int  0 0 0 0 0 0 0 0 0 61 ...
##  $ GSL_1981                            : int  0 0 0 0 0 0 0 0 0 15 ...
##  $ GSL_1982                            : int  0 0 0 0 0 0 0 0 0 0 ...
##  $ GSL_1983                            : int  0 0 0 0 0 0 0 0 0 0 ...
##  $ GSL_1984                            : int  0 0 0 0 0 0 0 0 0 50 ...
##  $ GSL_1985                            : int  0 0 0 0 0 0 0 0 0 53 ...
##  $ GSL_1986                            : int  0 0 0 0 0 0 0 0 0 0 ...
##  $ GSL_1987                            : int  0 0 0 0 0 0 0 0 0 54 ...
##  $ GSL_1988                            : int  0 0 0 0 0 0 0 0 0 50 ...
##  $ GSL_1989                            : int  0 0 0 0 0 0 0 0 0 18 ...
##  $ GSL_1990                            : int  0 0 0 0 0 0 0 0 0 19 ...
##  $ GSL_1991                            : int  0 0 0 0 0 0 0 0 0 37 ...
##  $ GSL_1992                            : int  0 0 0 0 0 0 0 0 0 0 ...
##  $ GSL_1993                            : int  0 0 0 0 5 5 0 5 5 13 ...
##  $ GSL_1994                            : int  0 0 0 0 0 0 0 0 0 0 ...
##  $ GSL_1995                            : int  0 0 0 0 0 0 0 0 0 12 ...
##  $ GSL_1996                            : int  0 0 0 0 0 0 0 0 0 70 ...
##  $ GSL_1997                            : int  0 0 0 0 0 0 0 0 0 43 ...
##  $ GSL_1998                            : int  0 0 0 0 0 0 0 0 0 26 ...
##  $ GSL_1999                            : int  0 0 0 0 10 10 0 10 10 57 ...
##  $ GSL_2000                            : int  0 0 0 0 0 0 0 0 0 57 ...
##  $ GSL_2001                            : int  0 0 0 0 0 0 0 0 0 68 ...
##  $ GSL_2002                            : int  0 0 0 0 0 0 0 0 0 61 ...
##  $ GSL_2003                            : int  0 0 0 0 0 0 0 0 0 67 ...
##  $ GSL_2004                            : int  0 0 0 0 0 0 0 0 0 55 ...
##  $ GSL_2005                            : int  0 0 0 0 17 17 0 17 17 35 ...
##  $ GSL_2006                            : int  0 0 0 0 17 17 0 17 17 58 ...
##  $ GSL_2007                            : int  3 3 3 3 23 23 3 23 23 68 ...
##  $ GSL_2008                            : int  26 26 26 26 43 43 26 43 43 60 ...
##  $ GSL_2009                            : int  32 32 32 32 41 41 32 41 41 24 ...
##  $ GSL_2010                            : int  0 0 0 0 10 10 0 10 10 85 ...
##  $ GSL_2011                            : int  17 17 17 17 34 34 17 34 34 0 ...
##  $ GSL_2012                            : int  19 19 19 19 44 44 19 44 44 0 ...
##  $ GSL_2013                            : int  0 0 0 0 0 0 0 0 0 8 ...
##  $ GST_1979                            : num  NA NA NA NA NA NA NA NA NA NA ...
##  $ GST_1980                            : num  NA NA NA NA NA ...
##  $ GST_1981                            : num  NA NA NA NA NA ...
##  $ GST_1982                            : num  NA NA NA NA NA NA NA NA NA NA ...
##  $ GST_1983                            : num  NA NA NA NA NA NA NA NA NA NA ...
##  $ GST_1984                            : num  NA NA NA NA NA ...
##  $ GST_1985                            : num  NA NA NA NA NA ...
##  $ GST_1986                            : num  NA NA NA NA NA NA NA NA NA NA ...
##  $ GST_1987                            : num  NA NA NA NA NA ...
##  $ GST_1988                            : num  NA NA NA NA NA ...
##  $ GST_1989                            : num  NA NA NA NA NA ...
##   [list output truncated]
```

```r
mostRecordedSP = DATA$cover %>% group_by(Name) %>% summarise(nobs = length(Name)) %>% arrange(desc(nobs))
selection = as.vector(t(mostRecordedSP[1:30, "Name"]))
selection = selection[-which(selection %in% c("Arctagrostis latifolia", "Persicaria vivipara", "Carex aquatilis", "Cassiope tetragona", "Luzula confusa", "Luzula nivalis", "Salix arctica", "Poa arctica", "Eriophorum angustifolium", "Petasites frigidus", "Vaccinium uliginosum", "Dryas integrifolia", "Stellaria longipes", "Saxifraga cernua", "Festuca brachyphylla"))]
```

N.B. I have quickly selected 14 species among the 30 having the most observations, with no problem with zero-days growing season in the previous years before an observation, and no problem in the dataset... **selection procedure to discuss**

## Prepare species specific datasets
Step 1: `create_dataset`:
<br>- only subsites where the species have been seen (then ok for N=1)
<br>- extract plot info
<br>- build transition table
<br>- merge with plot info
<br>- calculate interval (years)
<br>- tag transition types
<br>- combine with climate (5 years earlier average)
<br>- merge all
<br>Step 2: adjustments
<br>- Add anomalies
<br>- remove intervals>=10 years
<br>- select variables c("snowDays", "GSL", "GST", "SoilMoist", "TotalCover")
<br>- modify SoilMoist levels (-1, 0, 1)
<br>Step 3: prepare dataset to fit
<br>- separate colonisation and extinction datasets
<br>- scale data (not SoilMoist)
<br>- prepare data for LaplacesDemon fit (optional)
<br>- add event column for glm fit


```r
spDATA.list = lapply(selection, function(SPECIES){
  print(SPECIES)
  spDATA = dat.species(SPECIES, DATA)
  return(spDATA)
} )
names(spDATA.list) = selection
spDATA.check = lapply(spDATA.list, check.nas)
```

## Visualisation of transition tables

```r
lapply(spDATA.list, function(x) table(x$spdat$trType))
```

```
## $`Vaccinium vitis-idaea`
## 
##   AA COLO  EXT   PP 
##  168   18   13  647 
## 
## $`Carex bigelowii`
## 
##   AA COLO  EXT   PP 
##  194   53   38  490 
## 
## $`Ledum palustre`
## 
##   AA COLO  EXT   PP 
##  147   22   17  513 
## 
## $`Betula nana`
## 
##   AA COLO  EXT   PP 
##  191   19    5  510 
## 
## $`Eriophorum vaginatum`
## 
##   AA COLO  EXT   PP 
##  199   34   27  419 
## 
## $`Salix pulchra`
## 
##   AA COLO  EXT   PP 
##  287   50   31  376 
## 
## $`Eriophorum chamissonis`
## 
##   AA COLO  EXT   PP 
##   81   40   46  251 
## 
## $`Persicaria bistorta`
## 
##   AA COLO  EXT   PP 
##  296   77   60  183 
## 
## $`Dupontia fisheri`
## 
##   AA COLO  EXT   PP 
##  214   29   38  218 
## 
## $`Rubus chamaemorus`
## 
##   AA COLO  EXT   PP 
##  315   34   22  191 
## 
## $`Salix reticulata`
## 
##   AA COLO  EXT   PP 
##  491   14   24  161 
## 
## $`Geum rossii`
## 
##   AA COLO  EXT   PP 
##   16    4    8  153 
## 
## $`Empetrum nigrum`
## 
##   AA COLO  EXT   PP 
##  448   18   23  133 
## 
## $`Bistorta bistortoides`
## 
##   AA COLO  EXT   PP 
##   43   20   22   96 
## 
## $`Hierochloe alpina`
## 
##   AA COLO  EXT   PP 
##  603   39   36   61
```
Here we can see how many transitions we have for each species.

## Fit models

```r
spDATA.fit = lapply(spDATA.list, fit.species)
```

## Summary of model evaluation

```r
eval.models = lapply(spDATA.fit, function(glm.fit){
  eval.colo = eval.fit(glm.fit$colo, glm.fit$dat$colo, "colo.event")[-1]
  coeffs.colo = sort(abs(glm.fit$colo$coefficients[-1]), dec=T)
  vars.colo = paste(names(coeffs.colo), collapse=";")
  eval.ext = eval.fit(glm.fit$ext, glm.fit$dat$ext, "ext.event")[-1]
  coeffs.ext = sort(abs(glm.fit$ext$coefficients[-1]), dec=T)
  vars.ext = paste(names(coeffs.ext), collapse=";")
  return(list(colo.R2 = eval.colo$R2, colo.AUC = eval.colo$AUC, ext.R2 = eval.ext$R2, ext.AUC = eval.ext$AUC, vars.colo = vars.colo, vars.ext = vars.ext, coeffs.colo = coeffs.colo, coeffs.ext=coeffs.ext))
})

res.tab.eval = as.data.frame(do.call(rbind, lapply(eval.models, function(x)unlist(x[1:4]))))
res.tab.vars = as.data.frame(do.call(rbind, lapply(eval.models, function(x)unlist(x[5:6]))))
  
kable(res.tab.eval, caption = "model evaluation", digits = 2) %>% kable_styling(bootstrap_options = c("striped", "hover", "responsive"), full_width=FALSE)
```

<table class="table table-striped table-hover table-responsive" style="width: auto !important; margin-left: auto; margin-right: auto;">
<caption>model evaluation</caption>
 <thead>
  <tr>
   <th style="text-align:left;">   </th>
   <th style="text-align:right;"> colo.R2 </th>
   <th style="text-align:right;"> colo.AUC </th>
   <th style="text-align:right;"> ext.R2 </th>
   <th style="text-align:right;"> ext.AUC </th>
  </tr>
 </thead>
<tbody>
  <tr>
   <td style="text-align:left;"> Vaccinium vitis-idaea </td>
   <td style="text-align:right;"> 0.42 </td>
   <td style="text-align:right;"> 0.89 </td>
   <td style="text-align:right;"> 0.03 </td>
   <td style="text-align:right;"> 0.61 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> Carex bigelowii </td>
   <td style="text-align:right;"> 0.31 </td>
   <td style="text-align:right;"> 0.79 </td>
   <td style="text-align:right;"> 0.08 </td>
   <td style="text-align:right;"> 0.64 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> Ledum palustre </td>
   <td style="text-align:right;"> 0.14 </td>
   <td style="text-align:right;"> 0.67 </td>
   <td style="text-align:right;"> 0.04 </td>
   <td style="text-align:right;"> 0.60 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> Betula nana </td>
   <td style="text-align:right;"> 0.29 </td>
   <td style="text-align:right;"> 0.85 </td>
   <td style="text-align:right;"> 0.18 </td>
   <td style="text-align:right;"> 0.88 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> Eriophorum vaginatum </td>
   <td style="text-align:right;"> 0.02 </td>
   <td style="text-align:right;"> 0.53 </td>
   <td style="text-align:right;"> 0.07 </td>
   <td style="text-align:right;"> 0.70 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> Salix pulchra </td>
   <td style="text-align:right;"> 0.18 </td>
   <td style="text-align:right;"> 0.75 </td>
   <td style="text-align:right;"> 0.03 </td>
   <td style="text-align:right;"> 0.59 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> Eriophorum chamissonis </td>
   <td style="text-align:right;"> 0.22 </td>
   <td style="text-align:right;"> 0.74 </td>
   <td style="text-align:right;"> 0.06 </td>
   <td style="text-align:right;"> 0.65 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> Persicaria bistorta </td>
   <td style="text-align:right;"> 0.11 </td>
   <td style="text-align:right;"> 0.67 </td>
   <td style="text-align:right;"> 0.08 </td>
   <td style="text-align:right;"> 0.63 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> Dupontia fisheri </td>
   <td style="text-align:right;"> 0.24 </td>
   <td style="text-align:right;"> 0.80 </td>
   <td style="text-align:right;"> 0.25 </td>
   <td style="text-align:right;"> 0.80 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> Rubus chamaemorus </td>
   <td style="text-align:right;"> 0.00 </td>
   <td style="text-align:right;"> 0.50 </td>
   <td style="text-align:right;"> 0.00 </td>
   <td style="text-align:right;"> 0.50 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> Salix reticulata </td>
   <td style="text-align:right;"> 0.18 </td>
   <td style="text-align:right;"> 0.75 </td>
   <td style="text-align:right;"> 0.21 </td>
   <td style="text-align:right;"> 0.73 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> Geum rossii </td>
   <td style="text-align:right;"> 0.25 </td>
   <td style="text-align:right;"> 0.70 </td>
   <td style="text-align:right;"> 0.28 </td>
   <td style="text-align:right;"> 0.86 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> Empetrum nigrum </td>
   <td style="text-align:right;"> 0.16 </td>
   <td style="text-align:right;"> 0.77 </td>
   <td style="text-align:right;"> 0.11 </td>
   <td style="text-align:right;"> 0.67 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> Bistorta bistortoides </td>
   <td style="text-align:right;"> 0.20 </td>
   <td style="text-align:right;"> 0.71 </td>
   <td style="text-align:right;"> 0.04 </td>
   <td style="text-align:right;"> 0.59 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> Hierochloe alpina </td>
   <td style="text-align:right;"> 0.18 </td>
   <td style="text-align:right;"> 0.77 </td>
   <td style="text-align:right;"> 0.28 </td>
   <td style="text-align:right;"> 0.77 </td>
  </tr>
</tbody>
</table>

```r
# kable(res.tab.vars, caption = "selected variables")%>% kable_styling(bootstrap_options = c("striped", "hover", "responsive"), font_size = 10)
```
N.B. Some models do not fit well. The variables explain more or less depending on species but also on processes (extinction and colonisation).



## Selected variables and coefficient, sumnmary table

```r
coeffs.all.list = lapply(spDATA.fit, function(x){
  coe.colo = data.frame(summary(x$colo)$coefficients)
  coe.colo[,"sp"] = x$dat$sp
  coe.colo[,"var"] = rownames(coe.colo)
  coe.colo[,"process"] = "colonisation"
  coe.ext = data.frame(summary(x$ext)$coefficients)
  coe.ext[,"sp"] = x$dat$sp
  coe.ext[,"var"] = rownames(coe.ext)
  coe.ext[,"process"] = "extinction"
  coe = rbind(coe.colo[-1,], coe.ext[-1,])
  return(coe)
  })
coeffs.all = do.call(rbind, coeffs.all.list)
# str(coeffs.all)
```

## Number of selected variable per process, accross all species

```r
data.frame(unclass(table(coeffs.all$var, coeffs.all$process)))  %>% 
  kable(.) %>%
  kable_styling("striped")
```

<table class="table table-striped" style="margin-left: auto; margin-right: auto;">
 <thead>
  <tr>
   <th style="text-align:left;">   </th>
   <th style="text-align:right;"> colonisation </th>
   <th style="text-align:right;"> extinction </th>
  </tr>
 </thead>
<tbody>
  <tr>
   <td style="text-align:left;"> GSL </td>
   <td style="text-align:right;"> 11 </td>
   <td style="text-align:right;"> 6 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> GSL:SoilMoist </td>
   <td style="text-align:right;"> 5 </td>
   <td style="text-align:right;"> 3 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> GST </td>
   <td style="text-align:right;"> 9 </td>
   <td style="text-align:right;"> 6 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> GST:SoilMoist </td>
   <td style="text-align:right;"> 5 </td>
   <td style="text-align:right;"> 2 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> SoilMoist </td>
   <td style="text-align:right;"> 13 </td>
   <td style="text-align:right;"> 10 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> TotalCover </td>
   <td style="text-align:right;"> 11 </td>
   <td style="text-align:right;"> 7 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> TotalCover:SoilMoist </td>
   <td style="text-align:right;"> 9 </td>
   <td style="text-align:right;"> 4 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> snowDays </td>
   <td style="text-align:right;"> 10 </td>
   <td style="text-align:right;"> 10 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> snowDays:SoilMoist </td>
   <td style="text-align:right;"> 7 </td>
   <td style="text-align:right;"> 4 </td>
  </tr>
</tbody>
</table>

## Variable effect coefficients

This gives a summary of selected variables with significant impact on colonisation or extinction and their estimated coefficients.


```r
coeffs.all[which(coeffs.all$process == "colonisation" & coeffs.all$Pr...z..<0.1),] %>%
  ggplot(aes(sp, Estimate, fill = var)) +
  geom_col(position = "dodge") +
  theme_bw() + facet_wrap(~sp,scales = "free_x", ncol=4) +
  ggtitle("Colonisation process")
```

![](Main_analyse_files/figure-html/plot_coeffs-1.png)<!-- -->

```r
coeffs.all[which(coeffs.all$process == "extinction" & coeffs.all$Pr...z..<0.1),] %>%
  ggplot(aes(sp, Estimate, fill = var)) +
  geom_col(position = "dodge") +
  theme_bw() + facet_wrap(~sp,scales = "free_x", ncol=4) +
  ggtitle("Extinction process")
```

![](Main_analyse_files/figure-html/plot_coeffs-2.png)<!-- -->

## Lambda (1-ext/colo)
An overview of the performance of populations by site of by species


```r
all.sites = unique(DATA$cover[,c("SemiUniquePLOT", "YEAR", "SUBSITE", "SoilMoist", "TotalCover")])
pDATA = proj.dat.shape(all.sites, DATA$climato)
selectVars = c("snowDays", "GSL", "GST", "SoilMoist", "TotalCover")
pDATA.all = na.omit(pDATA[,c(selectVars,"SemiUniquePLOT", "YEAR", "SUBSITE") ])

pred.all = lapply(spDATA.fit, predictions, pred.data = pDATA.all, selectVars=selectVars)
pred.tab = lapply(pred.all, function(x)data.frame(sp = x$sp, lambda = x$lambda, YEAR = x$YEAR, SUBSITE = x$SUBSITE))
pred.all.together = do.call(rbind, pred.tab)

pred.all.together[-which(pred.all.together$sp == "Rubus chamaemorus"), ] %>%
  ggplot(aes(YEAR, lambda)) +
  geom_point(shape = 1) +
  geom_smooth() +
  facet_wrap( ~ sp, ncol = 4)
```

```
## `geom_smooth()` using method = 'loess' and formula 'y ~ x'
```

![](Main_analyse_files/figure-html/lambda, fig-TRUE-1.png)<!-- -->

```r
pred.all.together$SITE =  unlist( lapply(as.character(pred.all.together$SUBSITE), function(x){
  strsplit(x, ":")[[1]][[1]]
}))

pred.all.together[-which(pred.all.together$sp == "Rubus chamaemorus"), ] %>%
  ggplot(aes(SITE, lambda, sp)) + 
  theme(axis.text.x = element_text(angle=90, hjust=1)) +
  geom_boxplot() + geom_hline(yintercept = 0) +
  facet_wrap( ~ sp,scales = "free_x", ncol = 7)
```

![](Main_analyse_files/figure-html/lambda, fig-TRUE-2.png)<!-- -->

```r
pred.all.together[-which(pred.all.together$sp == "Rubus chamaemorus"), ] %>%
  ggplot(aes(SITE, lambda, sp)) +
  theme(axis.text.x = element_text(angle=90, hjust=1)) +
  geom_boxplot() + ylim (-1, 1) + geom_hline(yintercept = 0) +
  facet_wrap( ~ sp,scales = "free_x", ncol = 7)
```

![](Main_analyse_files/figure-html/lambda, fig-TRUE-3.png)<!-- -->

Simplified version focusing only on the sign of lambda

```r
pred.all.together$lambdaSign = ifelse(pred.all.together$lambda>0, "+", "-")

pred.all.together[-which(pred.all.together$sp == "Rubus chamaemorus"), ] %>%
  ggplot(aes(x=SITE, y=lambdaSign, fill= lambdaSign )) +
  theme(axis.text.x = element_text(angle=90, hjust=1)) +
  geom_bar(stat="identity")  + 
  facet_wrap( ~ sp,scales = "free_x", ncol = 7)
```

![](Main_analyse_files/figure-html/lambda_sign-1.png)<!-- -->



