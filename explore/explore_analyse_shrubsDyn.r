rm(list=ls())

# =======================================================================
#
# load climate
#
# =======================================================================
temp = read.table("shrubhub/ARCfunc_timeseries_temp.txt", h=T, sep="\t", encoding = "UTF-8", comment.char="", quote = "")
precip = read.table("shrubhub/ARCfunc_timeseries_prec.txt", h=T, sep="\t", encoding = "UTF-8", comment.char="", quote = "")
head(temp)
head(precip)

climato = read.table("shrubhub/TVC_SITE_SUBSITE_UPDATED2016_snow_GSL_GST_CHELSAclimatologies.txt", h=T, sep="\t", encoding = "UTF-8", comment.char="", quote = "")
head(climato)
climato$SiteSubsite = paste(climato$SITE, climato$SUBSITE, sep=":")

# =======================================================================
load("coverc_sub.rdata")
head(coverc.sub)
hist(coverc.sub$COVER)
summary(coverc.sub$COVER)

# filter out treatment plots
cover.sub = coverc.sub[which(coverc.sub$TRTMT =="CTL"),]

# filter out un-repeated plots
cover.sub = cover.sub[which(cover.sub$RepeatedPlots ==1),]

hist(asin(sqrt(cover.sub$COVER/100)), breaks =21, col = "lightblue")
summary(cover.sub$COVER)

# =======================================================================
#
# Species selection
#
# =======================================================================

# only species that can cover something enough
#---
MAX_COVER_THRESHOLD = 25
#---
sp.dom = unique(cover.sub$Name[which(cover.sub$COVER>=MAX_COVER_THRESHOLD)])
length(sp.dom)
cover.spDom = cover.sub[which(cover.sub$Name %in% sp.dom),]

hist(asin(sqrt(cover.spDom$COVER/100)), col = "lightblue", breaks=30)
summary(cover.spDom$COVER)


# remove presences when low abundance
#----
PA_COVER_TRESHOLD = 1
#----
cover.dom = cover.spDom[which(cover.spDom$COVER>PA_COVER_TRESHOLD),]
hist(asin(sqrt(cover.dom$COVER/100)), col = "lightblue", breaks=30)
summary(cover.dom$COVER)

length(unique(cover.dom$Name))
length(unique(cover.dom$SUBSITE))

hist(table(cover.dom$Name), breaks=50)
sort(table(cover.dom$Name))


# =======================================================================
#
# Species dataset
#
# =======================================================================
#----
SPECIES = "Vaccinium vitis-idaea"
# SPECIES = "Betula nana"
# SPECIES = "Salix arctica"
# SPECIES = "Salix pulchra"
# SPECIES = "Vaccinium uliginosum"
# ----

source("R_fct/shape_data.r")

VACVIT = create_dataset("Vaccinium vitis-idaea", cover.dom)


# =======================================================================
#
# Fit
#
# =======================================================================
spdat = VACVIT
spdat = spdat[spdat$interval<10,]
nrow(spdat)
selectVars = c("snowDays", "GSL", "GST", "N", "SoilMoist")

spdat$SoilMoist = factor(spdat$SoilMoist)
levels(spdat$SoilMoist) = c(1:3)
spdat$SoilMoist = as.numeric(as.character(spdat$SoilMoist))

require(LaplacesDemon)
source("R_fct/model_fct.r")

nvars = length(selectVars)
dat = scale(spdat[,selectVars])

parm.names = as.parm.names(list(aE=rep(0, 1+nvars), aC=rep(0, 1+nvars) ))
nbetas = length(parm.names)

MyData <- list(N = nrow(dat), dat=dat, mon.names = c("logLik") , parm.names= parm.names,
var.aE = 1:(nvars),
var.aC = 1:(nvars),
aE.pos = grep("aE", parm.names),
aC.pos = grep("aC", parm.names),
AA.pos = spdat$trType=="absent",
AP.pos = spdat$trType=="colonisation",
PA.pos = spdat$trType=="extinction",
PP.pos = spdat$trType=="present",
invlogit_ = invlogit_,
itime = spdat$interval,
density = 1,
# lik_fct = lik_dd,
nbetas = nbetas)

#-------
# test loglik function
#-------
set.seed(129837)
Initial.Values <- rnorm(length(MyData$parm.names), 0, 1)

loglik_fct(Initial.Values, MyData)$LP
Fit = LaplaceApproximation(Model=loglik_fct, Initial.Values, Data=MyData, Method="HAR", Iterations = 50000)
Consort(Fit.RDMH1)

set.seed(666)
Initial.Values <- as.initial.values(Fit.RDMH1)
Fit.RDMH1 <- LaplacesDemon(Model=loglik_fct, Data=MyData, Initial.Values,
     Covar=Fit.RDMH1$Covar, Iterations=30000, Status=11111, Thinning=30,
     Algorithm="RDMH", Specs=NULL)
Consort(Fit.RDMH1)

set.seed(666)
Initial.Values <- as.initial.values(Fit.RDMH1)
Fit.RDMH1 <- LaplacesDemon(Model=loglik_fct, Data=MyData, Initial.Values,
     Covar=Fit.RDMH1$Covar, Iterations=870000, Status=11070, Thinning=870,
     Algorithm="RDMH", Specs=NULL)
Consort(Fit.RDMH1)

save.image("Fit.mcmc.VACVIT")

# =======================================================================
#
# Plot
#
# =======================================================================
Fit.mcmc = Fit.RDMH1
vars = selectVars
all.vars = rep(selectVars,2)
# c(vars[mod$dat$var.aE[-1]-1],
#   vars[mod$dat$var.aS[-1]-1],
#   vars[mod$dat$var.bE[-1]-1],
#   vars[mod$dat$var.bS[-1]-1],
#   vars[mod$dat$var.dE[-1]-1],
#   vars[mod$dat$var.dS[-1]-1])

# Importing the ggplot2 library
library(ggplot2)
library(reshape2)
df = as.data.frame(Fit.mcmc$Posterior1)
head(df)
df.long= melt(df)
head(df.long)
df.long$par = unlist(lapply(df.long$variable, substr, start=1, stop=2))
df.long$index = unlist(lapply(df.long$variable, gsub, pattern="[^(0-9)]", replacement=""))
df.long2 = df.long[df.long$index>1,]
df.long2$var = unlist(lapply(1:nrow(df.long2), function(x){vars[[df.long2$par[x]]][as.numeric(df.long2$index[x])-1]}))
df.long2$var = factor(df.long2$var)
levels(df.long2$var) = all.vars
levels(df.long2$var) = vars
# df.long2$var = factor(df.long2$var, levels(df.long2$var)[c(8, 9, 4, 2,1,3,5,6,7)])
df.long2$par = factor(df.long2$par)
levels(df.long2$par)
 # = c("aC", "aD", "bC", "bD", "d")

 colos = c("lightblue", "steelblue", "grey50", "tomato", "orange")


# Create a Violin plot
p1 = ggplot(df.long2, aes(x = var, y = value, fill = var)) +
  geom_violin(trim= FALSE) +
  geom_boxplot(alpha=0.3, width=0.2, outlier.size = 0) +
  facet_wrap(~ par, ncol=3, dir="v", scales="free") +
  theme(axis.title.x=element_blank(),
        axis.text.x=element_blank(),
        axis.ticks.x=element_blank())

# pdf("../graphs/var_effects.pdf", heigh=8, width=12)
p1 + geom_hline(yintercept=0, linetype="dashed", color = "black") +
  labs(title="Posterior distributions", x="variable", y="effect") +
    scale_fill_manual(name="variables",
                       labels=levels(df.long2$var) ,
                       values = colos)
